# SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden
#
# SPDX-License-Identifier: CC0-1.0

clean: clean-build clean-pyc clean-test clean-venv ## remove all build, virtual environments, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

clean-venv:  # remove the virtual environment
	rm -rf venv

venv-install: clean
    # try to init virtualenv with `python`
    # OR init by `python3` if that failed
	python -m venv venv || python3 -m venv venv
	@echo "#################################"
	@echo "Activate the virtenv now by executing: . venv/bin/activate"
	@echo "#################################"

venv-init: venv-install
	venv/bin/pip install -e .
	venv/bin/pip install -r requirements.txt
	PYTHON_VERSION=$$(venv/bin/python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))'); \
	patch venv/lib/python$${PYTHON_VERSION}/site-packages/linkml_runtime/loaders/rdflib_loader.py < linkml_runtime_rdflib_loader.patch; \
	patch venv/lib/python$${PYTHON_VERSION}/site-packages/linkml_runtime/dumpers/rdflib_dumper.py < linkml_runtime_rdflib_dumper.patch

venv-init-dev: venv-init
	venv/bin/python -m pip install -e .[dev]

test:
	venv/bin/pytest .

test-formatting:
	venv/bin/flake8 kh_populator kh_populator_domain kh_populator_logic
