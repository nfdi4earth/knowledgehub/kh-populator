"""
Pipeline which adds Material from table covering existing educational
resources to the selected target.
"""

import logging
from datetime import datetime
from pandas import isnull as pd_isnull
from rdflib import URIRef
from rdflib import Literal
from rdflib import BNode
import pandas as pd
from n4e_kh_schema_py.n4eschema import (
    LearningResource,
    Organization,
    LearningResourceType,
    EducationalLevel,
    RDLStage,
    Kind,
)
from kh_populator.harvesting_consumers.ror_consumer import (
    upsert as ror_upsert,
)
from kh_populator import util
from kh_populator.util import (
    get_id,
    N4E_UPLOADER_SOURCE_SYSTEM,
    ROR_SOURCE_SYSTEM,
    Sources,
)
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_logic.language import (
    lang_code_2_to_3,
    lang_code_3_to_eu_vocab,
)
from kh_populator_logic.rdf import DFGFO, license_id_to_url_based_on_spdx

log = logging.getLogger(__name__)


def run_pipeline(target_handler: TargetHandler) -> None:
    edu_train = util.get_edu_train_dataframe()
    missing_organizations = []
    for _, row in edu_train.iterrows():
        learning_resource = LearningResource(
            id="tmp", name=Literal(row["Title"])
        )
        # RDM related subjects are in columns 2-10 (hardcoded(!))
        for column_name in row.keys()[1:9]:
            value = row[column_name]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.about.append(RDLStage(column_name))

        # ESS subjects are in columns 12 - 24 (hardcoded(!))
        for column_name in row.keys()[12:24]:
            value = row[column_name]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.subjectArea.append(DFGFO[column_name])

        description = row["Description"]
        if not pd.isna(description):
            learning_resource.description = description
        publisher_name = row["Publisher"]

        if (
            pd.isna(row["Publisher_ror_id"])
            or row["Publisher_ror_id"] == "none"
        ):
            learning_resource.publisher = Organization(
                id=BNode(), name=publisher_name
            )
        else:
            ror_id = row["Publisher_ror_id"]
            orga_iri = get_id(ROR_SOURCE_SYSTEM, ror_id)
            if target_handler.exists(orga_iri):
                learning_resource.publisher = orga_iri
            else:
                ror_upsert.apply_async([ror_id], queue=Sources.ROR.name)
                missing_organizations.append(ror_id)
                continue

        lang_code2 = row["Language"]
        lang_code3 = lang_code_2_to_3(lang_code2.lower())
        language = lang_code_3_to_eu_vocab(lang_code3)
        learning_resource.inLanguage = [URIRef(language)]

        # license and rights
        license_id = row["License"]
        if isinstance(license_id, str) and license_id.strip() != "N/A":
            license_url = license_id_to_url_based_on_spdx(license_id)
            if license_url:
                learning_resource.license = license_url
            else:
                log.warning(
                    f"Unable to termine license URL for {license_id} in "
                    + f"LearninResource import of {row['Title']}"
                )

        copyright = row["Copyright owner"]
        if isinstance(copyright, str) and len(copyright.strip()) > 0:
            learning_resource.copyrightNotice = copyright

        keywords_txt = row["Keywords"]
        if not pd_isnull(keywords_txt):
            learning_resource.keywords = [
                k.strip() for k in keywords_txt.split(",")
            ]
        learning_resource.url = row["Source"]
        year = row["Year"]
        if isinstance(year, str) and year.isnumeric():
            # Note that in the KH we would like to have the datePublished
            # in the format YYYY-mm-dd, not only the year, however this
            # restriction is temporarily disabled
            learning_resource.datePublished = str(year)
        else:
            try:
                date = datetime.strptime("02.04.2024", "%d.%m.%Y").date()
                # calling str serializes in YYYY-mm-dd format
                learning_resource.datePublished = str(date)
            except ValueError:
                pass

        # learningResourceType
        type_column_names = [
            "exercise",
            "slide",
            "narrative text",
            "quiz",
            "video",
        ]
        for type_ in type_column_names:
            value = row[type_]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.learningResourceType.append(
                    LearningResourceType(type_)
                )

        # competencyRequired
        required_skill = row["Requirement"]
        if isinstance(required_skill, str) and len(required_skill) > 0:
            learning_resource.competencyRequired = (
                required_skill.strip().split(",")
            )

        # educationalLevel
        level_column_names = [
            "Intro",
            "Beginner",
            "Intermediate",
            "Advanced",
        ]
        for level in level_column_names:
            value = row[level]
            if isinstance(value, str) and value.strip() == "x":
                learning_resource.educationalLevel = EducationalLevel(level)

        # contactPoint
        contact_point = row["Author/Instructor/Email/Phone/Contact page"]
        if isinstance(contact_point, str):
            if contact_point.count("@") == 1:
                contact = Kind(hasEmail=contact_point)
            elif contact_point.startswith("http"):
                contact = Kind(hasURL=contact_point)
            else:
                contact = Kind(fn=contact_point)  # fn=fullname
            learning_resource.contactPoint.append(contact)

        # sourceSystem Identifier
        identifier = row["Identifier"]
        learning_resource.sourceSystemID = identifier
        learning_resource.sourceSystem = N4E_UPLOADER_SOURCE_SYSTEM
        iri = get_id(N4E_UPLOADER_SOURCE_SYSTEM, identifier)
        learning_resource.id = iri
        if target_handler.exists(iri):
            target_handler.update(learning_resource)
        else:
            target_handler.create(learning_resource)
        print(f"Upserted {learning_resource.name}")

    if len(missing_organizations) > 0:
        print(
            "ROR metadata is being harvested - please wait a few minutes "
            + "then repeat this pipeline to finalize."
        )
        print(f"Missing organizations {missing_organizations}")
        exit()
    print("Pipeline finished successfully")
