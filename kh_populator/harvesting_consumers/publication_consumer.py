from celery.utils.log import get_task_logger

from kh_populator.celery_deploy import app
from kh_populator.util import get_id, Sources, PUB_SOURCE_SYSTEM
from kh_populator.n4e_task import N4ETask
from kh_populator_domain.doi import (
    harvest_doi_metadata_via_content_negotiation,
)

logger = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.PUB)
def upsert(self: app.task, doi: str):
    """
    :param doi: the doi of the publication upsert
    """
    iri = get_id(PUB_SOURCE_SYSTEM, doi)
    publication = harvest_doi_metadata_via_content_negotiation(doi)
    publication.id = iri
    if publication:
        if not self.target_handler.exists(iri):
            self.target_handler.create(publication)
        else:
            self.target_handler.update(publication)
    else:
        logger.warning(f"Failed to harvest publication: {doi}")
