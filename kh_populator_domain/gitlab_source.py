"""
This file contains function for harvesting metadata from the NFDI4Earth Gitlab.

Especially the LHB article harvesting function create various iterations, this
workflow needs a smart overhaul with the coming update into asynchronous
harvesting jobs.
"""

from typing import Dict


from rdflib import Literal, URIRef, BNode
from gitlab import Gitlab
from linkml_runtime.loaders import YAMLLoader

from kh_populator_logic.language import (
    lang_code_3_to_eu_vocab,
)
from kh_populator_logic.rdf import (
    DFGFO,
    UNESCO,
    DOI,
    license_id_to_url_based_on_spdx,
)
from n4e_kh_schema_py.n4eschema import (
    LHBArticle,
    Person,
    SoftwareSourceCode,
    Organization,
    CreativeWork,
)

GITLAB_AACHEN_URL = "https://git.rwth-aachen.de"

LHB_GITLAB_PROJECT_ID = 79252
LHB_SOURCE_SYSTEM_ID_PREFIX = "docs/"
LHB_DATA_BASE_URL = (
    "https://git.rwth-aachen.de/nfdi4earth/livinghandbook/"
    + "livinghandbook/-/raw/main/docs/"
)
LHB_PUBLIC_BASE_URL = (
    "https://git.rwth-aachen.de/nfdi4earth/livinghandbook/"
    + "livinghandbook/-/blob/main/"
)

N4E_INCUBATORS_GROUP_ID = 102889
N4E_PILOTS_GROUP_ID = 121394


def get_repos_in_group(url: str, group_id: int):
    group = Gitlab(url).groups.get(group_id)
    return group.projects.list(get_all=True)


def get_files_in_repo(url: str, project_id: int, path: str = ""):
    project = Gitlab(url).projects.get(project_id)
    tree = project.repository_tree(path=path, get_all=True)
    return [item for item in tree if item["type"] == "blob"]


def metadata_to_lhb_article(metadata: dict) -> LHBArticle:
    metadata["id"] = "tmp"
    if "author" in metadata:
        for author in metadata["author"]:
            author["id"] = BNode()
    article = YAMLLoader().load_any(metadata, LHBArticle)
    article.isPartOf = []

    if article.subjectArea:
        subject_areas = []
        for subject in article.subjectArea:
            if subject.startswith("dfgfo:"):
                subject_areas.append(DFGFO[subject.replace("dfgfo:", "")])
            elif subject.startswith("unesco:"):
                subject_areas.append(UNESCO[subject.replace("unesco:", "")])
        article.subjectArea = subject_areas

    if isinstance(article.inLanguage, list):
        # TODO: currently no hard validation whether the value is
        # actually an iso639 3 letter language code
        article.inLanguage = [
            lang_code_3_to_eu_vocab(lang) for lang in article.inLanguage
        ]

    licenses_reconciliated_uris = []
    for license_id in article.license:
        license_uri = license_id_to_url_based_on_spdx(license_id)
        if license_uri:
            licenses_reconciliated_uris.append(license_uri)
    article.license = licenses_reconciliated_uris
    return article


def cff_to_software_code(cff_dict: Dict):
    software_code = SoftwareSourceCode(id="tmp", name=cff_dict["title"])

    if "authors" in cff_dict:
        for author in cff_dict["authors"]:
            name = get_name(author)
            person = Person(id=BNode(), name=name)
            if "orcid" in author:
                person.orcidId = author["orcid"]
            if "affiliation" in author:
                affiliations = author["affiliation"]
                if not isinstance(affiliations, list):
                    affiliations = [affiliations]
                for affiliation in affiliations:
                    person.affiliation.append(
                        Organization(id=BNode(), name=affiliation)
                    )
    if "abstract" in cff_dict:
        software_code.description.append(cff_dict["abstract"])
    if "keywords" in cff_dict:
        for keyword in cff_dict["keywords"]:
            software_code.keywords.append(keyword)
    # TODO: License must be an URL to the SPDX entry
    if "license" in cff_dict:
        # TODO: validate that this always yields valid SPDX identifiers
        if isinstance(cff_dict["license"], list):
            license = URIRef(
                "http://spdx.org/licenses/" + cff_dict["license"][0]
            )
        else:
            license = URIRef("http://spdx.org/licenses/" + cff_dict["license"])
        software_code.license = license
    if "repository-code" in cff_dict:
        software_code.codeRepository = URIRef(cff_dict["repository-code"])

    if "identifiers" in cff_dict:
        # for incubators this should be the Zenodo DOI
        for identifier in cff_dict["identifiers"]:
            if identifier["type"] == "doi":
                software_code.identifier.append(DOI[identifier["value"]])
    if "references" in cff_dict:
        for reference in cff_dict["references"]:
            if "type" not in reference:
                continue
            if reference["type"] == "generic":
                continue
            if "title" in reference:
                c = CreativeWork(id=BNode(), name=Literal(reference["title"]))
            else:
                continue
            if "scope" in reference:
                c.description = reference["scope"]
            if "year" in reference:
                c.datePublished = reference["year"]
            for a in reference["authors"]:
                name = get_name(a)
                c.author.append(Person(id=BNode(), name=name))
            if "journal" in reference:
                c.publisher.append(
                    Organization(id=BNode(), name=reference["journal"])
                )
            software_code.citation.append(c)
    return software_code


def get_name(author):
    if "name" in author:
        name = author["name"]
    else:
        name = ""
        if "given-names" in author:
            name += author["given-names"]
        if "family-name" in author:
            name += author["family-names"]
        if name == "":
            raise ValueError(
                "At least name, given-names or family-names must be "
                + "present in CFF"
            )

    return name
