import requests
from shapely.geometry.polygon import orient
from shapely.wkt import loads
from rdflib import Literal, URIRef
from owslib.etree import etree
from owslib.iso import MD_Metadata
from n4e_kh_schema_py.n4eschema import (
    Dataset,
    Distribution,
    Location,
    Kind,
    PeriodOfTime,
    Person,
)
from kh_populator_domain.iso_gmd import csw_record_to_n4e


# NOTE:
# Currently works only as `pytest tests/domain/ogc.py` from the package
# root because of the relative import (see below)
def test_csw_record_to_n4e():
    gdi_record_id = "03956cb6-2276-11eb-adc1-0242ac120002"
    record_url = (
        "https://gdk.gdi-de.org/gdi-de/srv/api/records/"
        + f"{gdi_record_id}/formatters/xml?attachment=true"
    )
    xml_parsed = etree.fromstring(requests.get(record_url, timeout=10).content)
    csw_record = MD_Metadata(xml_parsed)
    record = csw_record_to_n4e(csw_record)
    rec_test = Dataset(
        id="https://registry.gdi-de.org/id/de.bund.bkg.csw/84288268-2272-11eb-adc1-0242ac120002",
        title=Literal("INSPIRE Statistical Units ATKIS-DLM250", lang="en"),
        description=Literal(
            "Statistical units of Germany, derived from the german digital landscape model at scale 1:250000. Mapped via EuroBoundaryMap to satisfy INSPIRE conformance. The dataset is available as Open Data.",
            lang="en",
        ),
        distribution=[
            Distribution(
                title=Literal(
                    'Dienst "INSPIRE-WMS Statistical Units ATKIS-DLM250" (GetCapabilities)',
                    lang="en",
                ),
                accessURL="https://sgx.geodatenzentrum.de/wms_dlm250_su_inspire?SERVICE=WMS&Request=GetCapabilities",
            ),
            Distribution(
                title=[
                    Literal(
                        'Dienst "INSPIRE-WFS Statistical Units ATKIS-DLM250" (GetCapabilities)',
                        lang="en",
                    )
                ],
                accessURL=[
                    "https://sgx.geodatenzentrum.de/wfs_dlm250_su_inspire?SERVICE=WFS&Request=GetCapabilities"
                ],
            ),
        ],
        keyword=[
            Literal("inspireidentifiziert", lang="en"),
            Literal("opendata", lang="en"),
        ],
        theme=[
            "https://www.eionet.europa.eu/gemet/en/concept/6760",
            "https://www.eionet.europa.eu/gemet/en/concept/8062",
            "https://www.eionet.europa.eu/gemet/en/concept/7059",
            "http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national",
            "http://data.europa.eu/bna/c_ac64a52d",
            "http://inspire.ec.europa.eu/theme/su",
        ],
        spatialCoverage=Location(
            boundingBox=Literal(
                "POLYGON((6.098033905029297 55.054344177246094,15.5785551071167 55.054344177246094,15.5785551071167 47.23942184448242,6.098033905029297 47.23942184448242,6.098033905029297 55.054344177246094))",
                datatype=URIRef(
                    "http://www.opengis.net/ont/geosparql#wktLiteral"
                ),
            ),
            geometry=Literal(
                orient(
                    loads(
                        "POLYGON((6.098033905029297 55.054344177246094,15.5785551071167 55.054344177246094,15.5785551071167 47.23942184448242,6.098033905029297 47.23942184448242,6.098033905029297 55.054344177246094))"
                    )
                ).wkt,
                datatype=URIRef(
                    "http://www.opengis.net/ont/geosparql#wktLiteral"
                ),
            ),
        ),
        contactPoint=Kind(
            fn=Literal(
                "Federal Agency for Cartography and Geodesy", lang="en"
            ),
            hasURL=["https://www.bkg.bund.de"],
            hasEmail=["mailto:dlz@bkg.bund.de"],
        ),
        language="http://publications.europa.eu/resource/authority/language/GER",
    )
    for org in record.publisher:
        org.id = "tmp"
    print(rec_test.theme)
    print(record.theme)
    assert rec_test == record


def test_csw_record_to_n4e_gdi_wdcc():
    # TODO: handle as relative import?
    path = "tests/data/gdi_dataset_wdcc.xml"
    xml_parsed = etree.parse(path)
    csw_record = MD_Metadata(xml_parsed)

    record = csw_record_to_n4e(csw_record)
    for org in record.publisher:
        org.id = "tmp"
    rec_test = Dataset(
        id="tmp",
        title=Literal(
            "HadGEM2-A model output prepared for CMIP5 amip, served by ESGF",
            lang="en-usa",
        ),
        contactPoint=Kind(
            fn=Literal("Chris Jones", lang="en-usa"),
            hasURL=["http://www.metoffice.gov.uk"],
        ),
        description=Literal("Moc description", lang="en-usa"),
        keyword=[
            Literal("CMIP5", lang="en-usa"),
            Literal("HadGEM2-A", lang="en-usa"),
            Literal("IPCC", lang="en-usa"),
            Literal("IPCC-AR5", lang="en-usa"),
            Literal("IPCC-DDC", lang="en-usa"),
            Literal("MOHC", lang="en-usa"),
            Literal("WGI", lang="en-usa"),
            Literal("amip", lang="en-usa"),
            Literal("climate simulation", lang="en-usa"),
        ],
        spatialCoverage=Location(
            boundingBox=Literal(
                "POLYGON((0.0 90.0,360.0 90.0,360.0 -90.0,0.0 -90.0,0.0 90.0))",
                datatype=URIRef(
                    "http://www.opengis.net/ont/geosparql#wktLiteral"
                ),
            ),
            geometry=Literal(
                "POLYGON ((0 90, 0 -90, 360 -90, 360 90, 0 90))",
                datatype=URIRef(
                    "http://www.opengis.net/ont/geosparql#wktLiteral"
                ),
            ),
        ),
        variableMeasured=[
            "toa_outgoing_longwave_flux",
            "surface_upward_sensible_heat_flux",
            "surface_snow_amount",
            "eastward_wind",
            "atmosphere_water_vapor_content",
            "surface_downwelling_shortwave_flux_in_air_assuming_clear_sky",
            "air_pressure",
            "height_above_reference_ellipsoid",
            "effective_radius_of_convective_cloud_liquid_water_particles",
            "wind_speed",
            "atmosphere_mass_content_of_seasalt_dry_aerosol",
            "northward_wind",
            "tendency_of_mass_fraction_of_stratiform_cloud_ice_in_air_due_to_bergeron_findeisen_process_from_cloud_liquid",
            "tendency_of_specific_humidity_due_to_convection",
            "mass_fraction_of_cloud_ice_in_air",
        ],
        temporal=[
            PeriodOfTime(
                endDate=Literal(
                    "2008-12-30",
                    datatype=URIRef("http://www.w3.org/2001/XMLSchema#date"),
                ),
                startDate=Literal(
                    "1978-09-01",
                    datatype=URIRef("http://www.w3.org/2001/XMLSchema#date"),
                ),
            )
        ],
        issued=Literal(
            "2014-11-18",
            datatype=URIRef("http://www.w3.org/2001/XMLSchema#date"),
        ),
    )

    record.id = "tmp"
    for org in record.publisher:
        org.id = "tmp"
    assert rec_test == record
